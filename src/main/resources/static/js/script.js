
function initForm() {
    console.log('Привет от JavaScript!');
    document.getElementById("form_submit").disabled = true;
}

async function checkEmail() {
    var email = document.getElementById('form_email').value;

    let url = 'http://localhost:8080/api/emailExistance/' + email;
    let response = await fetch(url);

    let isExists = await response.json();
    if (isExists) {
        let comment = "e-mail '" + email + "' is exists already";
        document.getElementById('email_comment').textContent = comment;
        document.getElementById("form_submit").disabled = true;
    } else {
        let comment = "e-mail '" + email + "' is NOT exists already";
        document.getElementById('email_comment').textContent = null;
        document.getElementById("form_submit").disabled = false;
    }
}

async function checkName() {
    var name = document.getElementById('form_name').value;

    if (null == name || name == '') {
        let comment = "Name is can't to be empty";
        document.getElementById('name_comment').textContent = comment;
        document.getElementById("form_submit").disabled = true;
    } else {
        document.getElementById('name_comment').textContent = null;
        document.getElementById("form_submit").disabled = false;
    }
}