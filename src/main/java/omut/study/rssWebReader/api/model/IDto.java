package omut.study.rssWebReader.api.model;

import java.util.UUID;

public interface IDto {
    public UUID getId();
}
