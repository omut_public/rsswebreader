package omut.study.rssWebReader.api.model;

import java.util.UUID;

public interface IEntity {
    public UUID getId();
}
