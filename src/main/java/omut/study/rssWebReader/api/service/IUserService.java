package omut.study.rssWebReader.api.service;

import omut.study.rssWebReader.model.dto.AuthorizeDto;
import omut.study.rssWebReader.model.dto.UserDto;

import java.util.List;
import java.util.UUID;

public interface IUserService {
    UserDto generateRandomUser();
    UserDto findUser(UUID id);
    List<UserDto> getUsers();
    UserDto save(UserDto user);
    boolean existsByEmail(String email);
    UUID authorizeUser(AuthorizeDto authoriseDto);
    boolean logoutUser(String sessionId);
}
