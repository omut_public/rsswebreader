package omut.study.rssWebReader.api.mapper;

import omut.study.rssWebReader.api.model.IDto;
import omut.study.rssWebReader.api.model.IEntity;

public interface IMapper<IENTITY extends IEntity, IDTO extends IDto> {
    IENTITY toEntity(IDTO dto);
    IDTO toDto(IENTITY entity);
}
