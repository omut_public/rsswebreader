package omut.study.rssWebReader.service;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import omut.study.rssWebReader.api.service.IUserService;
import omut.study.rssWebReader.exceptions.SessionNotFoundException;
import omut.study.rssWebReader.exceptions.UserNotFoundException;
import omut.study.rssWebReader.mapper.UserMapper;
import omut.study.rssWebReader.model.dto.AuthorizeDto;
import omut.study.rssWebReader.model.dto.UserDto;
import omut.study.rssWebReader.model.entity.SessionEntity;
import omut.study.rssWebReader.model.entity.UserEntity;
import omut.study.rssWebReader.model.enums.Gender;
import omut.study.rssWebReader.model.enums.SessionStatus;
import omut.study.rssWebReader.repository.SessionRepository;
import omut.study.rssWebReader.repository.UserRepository;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.time.LocalDate;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Slf4j
@Service
@AllArgsConstructor
public class UserServiceImpl implements IUserService {

    private UserRepository userRepository;
    private SessionRepository sessionRepository;
    private UserMapper userMapper;

    @Override
    public UserDto generateRandomUser() {
        UserDto userDto = UserDto.builder()
                .name("Tom")
                .surname("Stone")
                .email("test@test.ru")
                .gender(Gender.MALE)
                .birthday(LocalDate.now())
                .build();

        UserEntity userEntity = userMapper.toEntity(userDto);
        if (!userRepository.existsByEmail(userEntity.getEmail()))  {
            userRepository.save(userEntity);
        } else {
            Optional<UserEntity> optional = userRepository.findByEmail(userEntity.getEmail());
            if(optional.isPresent()) {
                userDto = userMapper.toDto(optional.get());
            }
        }

        return userDto;
    }

    @Override
    public UserDto findUser(UUID id) {
        return userRepository.findById(id).map(userMapper::toDto).orElse(null);
    }

    @Override
    public List<UserDto> getUsers() {
        log.info("amount users: {}", userRepository.count());
        List<UserEntity> users = userRepository.findAll();
        log.info("Load users: {}", users.size());
        return users.stream()
                .map(userMapper::toDto)
                .collect(Collectors.toList());
    }

    @Override
    public UserDto save(UserDto user) {
        UserEntity entity = userMapper.toEntity(user);
        log.info("Try save user entity: {}", entity);
        entity = userRepository.save(entity);
        log.info("Saved user entity: {}", entity);
        return userMapper.toDto(entity);
    }

    @Override
    public boolean existsByEmail(String email) {
        return userRepository.existsByEmail(email);
    }

    @Override
    public UUID authorizeUser(AuthorizeDto authoriseDto) {

        log.info("authorizeUser: {}", authoriseDto);
        UserEntity userEntity = userRepository.findByEmail(authoriseDto.getLoginOrEmail()).orElse(null);
        if (Objects.isNull(userEntity)) {
            throw new UserNotFoundException("User with this login or email [" + authoriseDto.getLoginOrEmail() + "] not found.");
        }

        SessionEntity session = SessionEntity.builder().user(userEntity).status(SessionStatus.OPEN).build();

        session = sessionRepository.save(session);
        return session.getId();
    }

    @Override
    public boolean logoutUser(String sessionId) {
        log.info("SessionID: {}", sessionId);
        SessionEntity sessionEntity = sessionRepository.findById(UUID.fromString(sessionId)).orElse(null);
        if (Objects.isNull(sessionEntity)) {
            throw new SessionNotFoundException("Session with ID [" + sessionId + "] not found.");
        }
        try {
            sessionEntity.setStatus(SessionStatus.CLOSED);
            sessionEntity.setClosed(Instant.now());
            sessionRepository.save(sessionEntity);
            return true;
        } catch (Exception e) {
            log.warn("Couldn't close session with id: {}", sessionId);
            return false;
        }
    }
}
