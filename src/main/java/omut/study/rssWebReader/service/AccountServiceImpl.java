package omut.study.rssWebReader.service;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import omut.study.rssWebReader.api.service.IAccountService;
import omut.study.rssWebReader.repository.UserRepository;
import org.springframework.stereotype.Service;

import static omut.study.rssWebReader.model.Constants.NOP;

@Slf4j
@Service
@AllArgsConstructor
public class AccountServiceImpl implements IAccountService {

    private UserRepository userRepository;

    @Override
    public void authorize() {
        log.info("AccountServiceImpl authorize: {}", NOP);
    }

    @Override
    public void registry() {
        log.info("AccountServiceImpl registry: {}", NOP);
    }
}