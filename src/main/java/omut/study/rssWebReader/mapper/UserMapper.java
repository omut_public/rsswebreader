package omut.study.rssWebReader.mapper;

import omut.study.rssWebReader.api.mapper.IMapper;
import omut.study.rssWebReader.model.dto.UserDto;
import omut.study.rssWebReader.model.entity.UserEntity;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.TimeZone;

@Service
public class UserMapper implements IMapper<UserEntity, UserDto> {

    @Override
    public UserEntity toEntity(UserDto dto) {
        return UserEntity.builder()
                .name(dto.getName())
                .surname(dto.getSurname())
                .email(dto.getEmail())
                .gender(dto.getGender())
                .birthday(dto.getBirthday())
                .password(dto.getPassword())
                .login(dto.getLogin())
                .status(dto.getStatus())
                .build();
    }

    @Override
    public UserDto toDto(UserEntity entity) {
        LocalDate created = entity.getCreated() == null
                ? null
                : entity.getCreated().atZone(TimeZone.getDefault().toZoneId()).toLocalDate();

        return UserDto.builder()
                .id(entity.getId())
                .name(entity.getName())
                .surname(entity.getSurname())
                .email(entity.getEmail())
                .gender(entity.getGender())
                .created(created)
                .birthday(entity.getBirthday())
                .build();
    }
}
