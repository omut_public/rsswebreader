package omut.study.rssWebReader.model.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
public enum Gender {
    MALE(0),
    FEMALE(1);

    @Getter
    private int id;
}
