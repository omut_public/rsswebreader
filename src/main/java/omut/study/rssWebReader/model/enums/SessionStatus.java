package omut.study.rssWebReader.model.enums;

public enum SessionStatus {
    OPEN,
    CLOSED
}
