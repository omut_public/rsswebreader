package omut.study.rssWebReader.model.enums;

public enum UserStatus {
    ACTUAL,
    BLOCKED
}
