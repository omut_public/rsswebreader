package omut.study.rssWebReader.model.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AuthorizeDto {

    @NotBlank
    private String loginOrEmail;
    @NotBlank
    private String password;
}
