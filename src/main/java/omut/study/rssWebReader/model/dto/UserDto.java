package omut.study.rssWebReader.model.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import omut.study.rssWebReader.api.model.IDto;
import omut.study.rssWebReader.model.enums.Gender;
import omut.study.rssWebReader.model.enums.UserStatus;

import javax.validation.constraints.NotBlank;
import java.time.LocalDate;
import java.util.UUID;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UserDto implements IDto {
    private UUID id;

    @NotBlank
    private String name;
    @NotBlank
    private String surname;
    @NotBlank
    private String email;
    private Gender gender;
    private LocalDate created;
    private LocalDate birthday;
    @NotBlank
    private String login;
    @NotBlank
    private String password;
    private UserStatus status;
}
