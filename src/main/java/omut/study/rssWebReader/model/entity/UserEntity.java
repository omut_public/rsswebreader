package omut.study.rssWebReader.model.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import omut.study.rssWebReader.api.model.IEntity;
import omut.study.rssWebReader.model.enums.Gender;
import omut.study.rssWebReader.model.enums.UserStatus;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.time.Instant;
import java.time.LocalDate;
import java.util.UUID;

@Data
@Builder
@Entity(name="users")
@NoArgsConstructor
@AllArgsConstructor
public class UserEntity implements IEntity {

    @Id
    @GeneratedValue
    private UUID id;

    private String name;

    private String surname;

    @Column(unique = true)
    private String email;

    private Gender gender;

    private LocalDate birthday;

    @Basic
    @CreationTimestamp
    @Column(updatable = false)
    private Instant created;

    private Instant updated;

    private String login;

    private String password;

    private UserStatus status;
}

