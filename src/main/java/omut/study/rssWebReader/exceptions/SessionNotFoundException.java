package omut.study.rssWebReader.exceptions;

public class SessionNotFoundException extends RuntimeException{

    public SessionNotFoundException(String message) {
        super(message);
    }

}
