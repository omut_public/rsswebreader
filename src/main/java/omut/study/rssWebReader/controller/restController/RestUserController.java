package omut.study.rssWebReader.controller.restController;

import lombok.extern.slf4j.Slf4j;
import omut.study.rssWebReader.api.service.IUserService;
import omut.study.rssWebReader.model.dto.TestDto;
import omut.study.rssWebReader.model.dto.UserDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

@Slf4j
@RequestMapping(value = "/", produces = "application/json;charset=UTF-8")
@RestController
public class RestUserController {

    @Value("${rss.test.message}")
    private String testMessage;

    @Autowired
    private IUserService userService;

    @GetMapping("/api/generateUser")
    public UserDto generateUser() {
        return userService.generateRandomUser();
    }

    @GetMapping("/api/emailExistance/{email}")
    public Boolean checkEmailExistance(@PathVariable("email") String email) {
        log.info("GET: /api/emailExistance/{}", email);
        return email == null? false : userService.existsByEmail(email);
    }

    @GetMapping("/api/user/{id}")
    public UserDto getUser(@PathVariable("id") UUID id) {
        log.info("Test message: {}", testMessage);

        return userService.findUser(id);
    }

    @PostMapping( "/api/registry" )
    public UserDto registry( @RequestBody UserDto user
    ) {
        log.info("POST: /api/registry request: {}", user);

        String name = user.getName();
        String surname = user.getSurname();
        String email = user.getEmail();
        String gender = user.getGender().name();

        log.info("Name Surname: {} {} {}", name, surname, gender);
        log.info("UserDto: {}", user);

        if (name != null && name.length() > 0 //
                && surname != null && surname.length() > 0
                && email != null && email.length() > 0) {
            user.setId(UUID.randomUUID());
            UserDto suser = userService.save(user);
            log.info("Saved and returning UserDto: {}", suser);
            return suser;
        }

        log.info("Not save UserDto, returning null");

        return null;
    }

    @GetMapping("/rest_test")
    public ResponseEntity<Object> test() {
        return new ResponseEntity<>(TestDto.builder().message("test").build(), HttpStatus.OK);
    }
}