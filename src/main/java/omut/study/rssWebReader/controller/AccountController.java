package omut.study.rssWebReader.controller;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import omut.study.rssWebReader.api.service.IAccountService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Slf4j
@Controller
@AllArgsConstructor
public class AccountController {

    private IAccountService accountService;

    @GetMapping( "/authorize")
    public String authorize(Model model) {
        accountService.authorize();
        return "authorize";
    }

    @GetMapping( "/registry")
    public String registry(Model model) {
        accountService.registry();

        return "registry";
    }
}
