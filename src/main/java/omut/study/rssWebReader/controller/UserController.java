package omut.study.rssWebReader.controller;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import omut.study.rssWebReader.api.service.IUserService;
import omut.study.rssWebReader.model.dto.AuthorizeDto;
import omut.study.rssWebReader.model.dto.LogoutDto;
import omut.study.rssWebReader.model.dto.UserDto;
import omut.study.rssWebReader.model.enums.Gender;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.validation.Valid;
import java.time.Instant;
import java.util.List;
import java.util.UUID;

@Slf4j
@Controller
@AllArgsConstructor
public class UserController {

    private IUserService userService;

    @GetMapping( "/users")
    public String authorize(Model model) {
        log.info("GET: /users");

        List<UserDto> users = userService.getUsers();
        model.addAttribute("users", users);
        return "users";
    }

    @GetMapping( "/generateRandomUser")
    public String generateUser(Model model) {
        UserDto user = userService.generateRandomUser();
        model.addAttribute("user", user);
        model.addAttribute("created", Instant.now());
        return "generateRandomUser";
    }

    @RequestMapping(value = { "/registryUser" }, method = RequestMethod.POST)
    public String saveUser(Model model, @ModelAttribute("registryUser") @Valid UserDto registryUser) {
        log.info("POST: /saveUser body: {}", registryUser);

        String name = registryUser.getName();
        String surname = registryUser.getSurname();
        String email = registryUser.getEmail();
        String gender = Gender.FEMALE.name();

        log.info("Name Surname: {} {} {}", name, surname, gender);
        log.info("UserDto: {}", registryUser);

        if (name != null && name.length() > 0 //
                && surname != null && surname.length() > 0
                && email != null && email.length() > 0) {
            UserDto user = userService.save(registryUser);
            log.info("Saved UserDto: {}. Redirecting to /authorize", user);
            return "redirect:/authorize";
        }

        log.info("Not save UserDto: {}. Redirecting to /registry");

        return "registry";
    }

    @RequestMapping(value = { "/authorizeUser" }, method = RequestMethod.POST)
    public String authorizeUser(Model model, @ModelAttribute("registryUser") @Valid AuthorizeDto authorizeUser) {
        log.info("POST: /authorizeUser body: {}", authorizeUser);

        UUID sessionId = userService.authorizeUser(authorizeUser);
        model.addAttribute("sessionId", sessionId);
        return "main";
    }

    @RequestMapping(value = { "/logoutUser" }, method = RequestMethod.POST)
    public String logoutUserPost(Model model, @ModelAttribute("registryUser") LogoutDto logoutFto) {
        log.info("POST: /logoutUser body: {}", logoutFto);

        boolean isLogout = userService.logoutUser(logoutFto.getSessionId());
        log.info("Logout session: {}, session is closed? {}", logoutFto.getSessionId(), isLogout);
        return "index";
    }

}
