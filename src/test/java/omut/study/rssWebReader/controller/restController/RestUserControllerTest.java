package omut.study.rssWebReader.controller.restController;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import omut.study.rssWebReader.model.dto.TestDto;
import omut.study.rssWebReader.model.entity.UserEntity;
import omut.study.rssWebReader.repository.UserRepository;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@Slf4j
@SpringBootTest
@AutoConfigureMockMvc
@TestPropertySource("classpath:application-test.properties")
@Sql({"/sql_scripts/delete_users.sql", "/sql_scripts/insert_users.sql"})
class RestUserControllerTest {

    @Autowired
    private MockMvc mock;

    @Autowired
    private UserRepository repository;

    @Value("${rss.test.message}")
    private String testMessage;

    @Value("${spring.datasource.url}")
    private String db;

    private MvcResult response;
    private ObjectMapper objectMapper = new ObjectMapper();

    @BeforeEach
    void setUp() {
        log.info("Test message: {}", testMessage);
        log.info("Test db: {}", db);
        UserEntity ue = repository.save(UserEntity.builder().name("test name").build());
        log.info("UE: {}", ue);
        repository.findAll().forEach(System.out::println);
        log.info("Users: [{}]", repository.findAll());
        log.info("Amount Users: {}", repository.findAll().size());
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void generateUser() {
        log.info("Test for api 'GET:/api/generateUser'");
        assertTrue(true);
    }

    @Test
    void getUser() {
        log.info("Test for api 'GET:/api/user/{id}'");
        log.info("Test message: {}", testMessage);
        assertTrue(true);

    }

    @Test
    void test() throws Exception {
        response = mock.perform(get( "/rest_test/"))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn();

        TestDto result =
                objectMapper.readValue(response.getResponse().getContentAsString(), TestDto.class);
        assertEquals("test", result.getMessage());
    }

}