INSERT INTO public.users
(id, birthday,  created, email, gender, name, surname)
VALUES
    ('8b5e6cc8-3cdf-402a-ba32-e1ebe8ade65c', '1987-12-23', now(), 'test1@test.test', 1, 'User_1', 'Test user 1'),
    ('f65d81f9-20d2-44c9-8055-68e12c63d397', '1994-02-17', now(), 'test2@test.test', 0, 'User_2', 'Test user 2'),
    ('30871a06-0e8b-4f2a-8948-3c76059e1e20', '1999-07-10', now(), 'test3@test.test', 1, 'User_3', 'Test user 3')
;